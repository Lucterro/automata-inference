/*
  eval.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/05/02 : Version 0
*/

#ifndef _eval_h
#define _eval_h

#include <base/solution.h>
#include <base/sample.h>

/*
    Generic evaluation function of DFA solution

    Evaluation function requires a sample to evaluate a solution
*/
template< class Fitness >
class Eval {
public:
    Eval(Sample & _sample) : sample(_sample) {
    }

    virtual void operator()(Solution<Fitness> & _solution) = 0;

    // sample to evaluate a solution
    Sample & sample;
};

class jsonEval : public Eval<double> {
  public:
  jsonEval(Sample sample) : Eval(sample){}
  
};

#endif