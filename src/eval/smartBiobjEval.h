/*
  smartBiobjEval.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/05/02 : Version 0
*/

#ifndef _smartBiobjEval_h
#define _smartBiobjEval_h

#include <vector>
#include <random>
#include <base/solution.h>
#include <base/sample.h>
#include <eval/eval.h>

/*
    Evaluation function of DFA solution:
      Read the words the sample, 
      According to the accept states reaching by the words,
      The accept state is set in order to maximise the ratio of correct words.
      The fitness is a pair :
         - the ratio of words from the sample which are corrected computed
         - number of states used

*/
class SmartBiobjEval : public Eval< std::pair<double, unsigned> > {
public:
    typedef std::pair<double, unsigned> Fitness;

    using Eval::sample;

    SmartBiobjEval(std::mt19937 & _gen, Sample & _sample) : Eval(_sample), gen(_gen) {
    }

    /*
      Set the fitness value, and also the acceptState of the DFA solution
    */
    virtual void operator()(Solution<Fitness> & _solution) {
      std::vector<unsigned> accepted_finalStates(_solution.acceptStates.size(), 0);
      std::vector<unsigned> rejected_finalStates(_solution.acceptStates.size(), 0);

      std::vector<unsigned> ni(_solution.nStates, 0);

      int fstate;
      for(auto word : sample.set) {
        fstate = _solution.read(word, ni);
        if (0 <= fstate) {
          if (word.accept) 
            accepted_finalStates[fstate]++;
          else
            rejected_finalStates[fstate]++;
        }
      }

      unsigned correct = 0;
      for(unsigned i = 0; i < accepted_finalStates.size(); i++) {
        if (accepted_finalStates[i] < rejected_finalStates[i]) {
          _solution.acceptStates[i] = false;
          correct += rejected_finalStates[i];
        } else {
          if (rejected_finalStates[i] < accepted_finalStates[i]) {
            _solution.acceptStates[i] = true;
            correct += accepted_finalStates[i];          
          } else {
            // break ties at random
            if (runif(gen) < 0.5) {
              _solution.acceptStates[i] = false;
              correct += rejected_finalStates[i];
            } else {
              _solution.acceptStates[i] = true;
              correct += accepted_finalStates[i];
            }
          }
        }
      }

      unsigned used = 0;
      for(auto i : ni) {
        if (i != 0)
          used++;
      }

      _solution.fitness(std::pair<double, unsigned>(((double) correct) / (double) sample.set.size(), used));
    }

    // random generator
    std::uniform_real_distribution<> runif ; //(0.0, 1.0);
    std::mt19937 & gen;

};


#endif