#include <iostream>
#include <fstream>
#include <random>

using namespace std;

#include <base/sample.h>
#include <base/solution.h>
#include <base/hillClimber.h>
#include <base/recuitSim.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>

int main(int argc, char ** argv) {  
    cout << "/**************** Hill Climber ****************/" << endl;
    /*Paramètres :
    * Nom du fichier (sample)
    * Graine aléatoire
    * Nombre d'évaluations
    */
    ofstream myfile;
    HillClimber hc;

    myfile.open ("hc_dfa_4_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain0 = "dfa_4_"+to_string(i)+"_0.01_train-sample.json";
            string fTest0 = "dfa_4_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain0 << " & " << fTest0 << endl;
            myfile << hc.run(fTrain0, i, 50000) << '|' << hc.run(fTest0, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_4_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain1 = "dfa_4_"+to_string(i)+"_0.05_train-sample.json";
            string fTest1 = "dfa_4_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain1 << " & " << fTest1 << endl;
            myfile << hc.run(fTrain1, i, 50000) << '|' << hc.run(fTest1, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_4_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain2 = "dfa_4_"+to_string(i)+"_0.1_train-sample.json";
            string fTest2 = "dfa_4_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain2 << " & " << fTest2 << endl;
            myfile << hc.run(fTrain2, i, 50000) << '|' << hc.run(fTest2, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open ("hc_dfa_8_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain3 = "dfa_8_"+to_string(i)+"_0.01_train-sample.json";
            string fTest3 = "dfa_8_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain3 << " & " << fTest3 << endl;
            myfile << hc.run(fTrain3, i, 50000) << '|' << hc.run(fTest3, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_8_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain4 = "dfa_8_"+to_string(i)+"_0.05_train-sample.json";
            string fTest4 = "dfa_8_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain4 << " & " << fTest4 << endl;
            myfile << hc.run(fTrain4, i, 50000) << '|' << hc.run(fTest4, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_8_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain5 = "dfa_8_"+to_string(i)+"_0.1_train-sample.json";
            string fTest5 = "dfa_8_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain5 << " & " << fTest5 << endl;
            myfile << hc.run(fTrain5, i, 50000) << '|' << hc.run(fTest5, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_16_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain6 = "dfa_16_"+to_string(i)+"_0.01_train-sample.json";
            string fTest6 = "dfa_16_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain6 << " & " << fTest6 << endl;
            myfile << hc.run(fTrain6, i, 50000) << '|' << hc.run(fTest6, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open ("hc_dfa_16_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain7 = "dfa_16_"+to_string(i)+"_0.05_train-sample.json";
            string fTest7 = "dfa_16_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain7 << " & " << fTest7 << endl;
            myfile << hc.run(fTrain7, i, 50000) << '|' << hc.run(fTest7, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_16_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain8 = "dfa_16_"+to_string(i)+"_0.1_train-sample.json";
            string fTest8 = "dfa_16_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain8 << " & " << fTest8 << endl;
            myfile << hc.run(fTrain8, i, 50000) << '|' << hc.run(fTest8, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_32_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain9 = "dfa_32_"+to_string(i)+"_0.01_train-sample.json";
            string fTest9 = "dfa_32_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain9 << " & " << fTest9 << endl;
            myfile << hc.run(fTrain9, i, 50000) << '|' << hc.run(fTest9, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open ("hc_dfa_32_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain10 = "dfa_32_"+to_string(i)+"_0.05_train-sample.json";
            string fTest10 = "dfa_32_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain10 << " & " << fTest10 << endl;
            myfile << hc.run(fTrain10, i, 50000) << '|' << hc.run(fTest10, i, 50000) << endl;
        }
    }
    myfile.close();

    myfile.open("hc_dfa_32_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain11 = "dfa_32_"+to_string(i)+"_0.1_train-sample.json";
            string fTest11 = "dfa_32_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain11 << " & " << fTest11 << endl;
            myfile << hc.run(fTrain11, i, 50000) << '|' << hc.run(fTest11, i, 50000) << endl;
        }
    }
    myfile.close();

    //Execution manuelle :
    /*HillClimber hillClimber;
    hillClimber.run(argv[1], atoi(argv[2]), atoi(argv[3]));*/

    cout << endl;
    cout << "/**************** Recuit simule ****************/" << endl;
    /*Paramètres :
    * Nom du fichier (sample)
    * Graine aléatoire
    * Nombre d'évaluations
    * Température de départ
    * Step-size
    * Temperature coefficient
    */

   //Execution manuelle :
   /*RecuitSim rS;
   recuitSim.run(argv[1], atoi(argv[2]), atoi(argv[3]), stod(argv[4]), atoi(argv[5]), stod(argv[6]));*/

    RecuitSim rS;

    myfile.open ("rS_dfa_4_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain0 = "dfa_4_"+to_string(i)+"_0.01_train-sample.json";
            string fTest0 = "dfa_4_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain0 << " & " << fTest0 << endl;    
            myfile <<  rS.run(fTrain0, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest0, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_4_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain1 = "dfa_4_"+to_string(i)+"_0.05_train-sample.json";
            string fTest1 = "dfa_4_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain1 << " & " << fTest1 << endl;    
            myfile <<  rS.run(fTrain1, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest1, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_4_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain2 = "dfa_4_"+to_string(i)+"_0.1_train-sample.json";
            string fTest2 = "dfa_4_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain2 << " & " << fTest2 << endl;    
            myfile <<  rS.run(fTrain2, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest2, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open ("rS_dfa_8_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain3 = "dfa_8_"+to_string(i)+"_0.01_train-sample.json";
            string fTest3 = "dfa_8_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain3 << " & " << fTest3 << endl;    
            myfile <<  rS.run(fTrain3, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest3, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_8_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain4 = "dfa_8_"+to_string(i)+"_0.05_train-sample.json";
            string fTest4 = "dfa_8_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain4 << " & " << fTest4 << endl;    
            myfile <<  rS.run(fTrain4, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest4, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_8_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain5 = "dfa_8_"+to_string(i)+"_0.1_train-sample.json";
            string fTest5 = "dfa_8_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain5 << " & " << fTest5 << endl;    
            myfile <<  rS.run(fTrain5, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest5, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open ("rS_dfa_16_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain6 = "dfa_16_"+to_string(i)+"_0.01_train-sample.json";
            string fTest6 = "dfa_16_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain6 << " & " << fTest6 << endl;    
            myfile <<  rS.run(fTrain6, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest6, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_16_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain7 = "dfa_16_"+to_string(i)+"_0.05_train-sample.json";
            string fTest7 = "dfa_16_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain7 << " & " << fTest7 << endl;    
            myfile <<  rS.run(fTrain7, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest7, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_16_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain8 = "dfa_16_"+to_string(i)+"_0.1_train-sample.json";
            string fTest8 = "dfa_16_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain8 << " & " << fTest8 << endl;    
            myfile <<  rS.run(fTrain8, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest8, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open ("rS_dfa_32_0.01.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain9 = "dfa_32_"+to_string(i)+"_0.01_train-sample.json";
            string fTest9 = "dfa_32_"+to_string(i)+"_0.01_test-sample.json";

            //Train & test
            cout << fTrain9 << " & " << fTest9 << endl;    
            myfile <<  rS.run(fTrain9, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest9, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_32_0.05.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain10 = "dfa_32_"+to_string(i)+"_0.05_train-sample.json";
            string fTest10 = "dfa_32_"+to_string(i)+"_0.05_test-sample.json";

            //Train & test
            cout << fTrain10 << " & " << fTest10 << endl;    
            myfile <<  rS.run(fTrain10, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest10, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();

    myfile.open("rS_dfa_32_0.1.csv");
    myfile << "Fitness_train" << '|' << "Fitness_test" << endl;

    for(int j = 0; j < 5; j++){
        for (int i = 0; i < 30; i++) {
            string fTrain11 = "dfa_32_"+to_string(i)+"_0.1_train-sample.json";
            string fTest11 = "dfa_32_"+to_string(i)+"_0.1_test-sample.json";

            //Train & test
            cout << fTrain11 << " & " << fTest11 << endl;    
            myfile <<  rS.run(fTrain11, i, 50000, 1, 100, 0.95) << '|' << rS.run(fTest11, i, 50000, 1, 100, 0.95) << endl;
        }
    }
    myfile.close();
} //End_main