#ifndef _HILLCLIMBER_H
#define _HILLCLIMBER_H

using namespace std;

#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <sstream>
#include <utility>
#include <string>

#include <base/sample.h>
#include <base/solution.h>
#include <eval/smartEval.h>
#include <rapidjson/document.h>

class HillClimber : public DFA {
private:
    string _fileName;

public:
    HillClimber(){}

    vector<string> chararacter(string str){
        vector<string> character;
        istringstream strStream(str);

        for(string element; std::getline(strStream, element, '_'); ){
            character.push_back(move(element));
        }
        return character;
    }

    double run(string fileName, int seed, int _nbEvalMax){
        _fileName = fileName;
        int nbEval = 0;
        int state = stoi(chararacter(fileName)[1]);

        string url = "/home/lucas/Bureau/Projets_RO/automata-inferenceLucas/instances/" + _fileName;
        Sample sample(url.c_str());

        Solution<double> sol(state, 2);

        //Random engine init using seed. Generates 2 int randoms
        std::mt19937 mt_rand (seed);
        std::uniform_int_distribution<int> dist(0,state-1);     
        std::uniform_int_distribution<int> distribution(1,(state-1));
        
        for (int i = 0; i < sol.function.size(); ++i) {
            for (int j = 0; j < sol.function[i].size(); ++j) {
                sol.function[i][j] = dist(mt_rand);
            }
        }

        SmartEval eval(mt_rand, sample);

        eval(sol);

        double fitnessMax;
        bool arret = false;

        while(nbEval < _nbEvalMax && arret == false){
            fitnessMax = sol.fitness();

            for(int i = 0; i < state; ++i){
                for(int j = 0; j < 2; ++j){
                    sol.function[i][j] =  (sol.function[i][j] + distribution(mt_rand) ) % state;
                    eval(sol);
                    nbEval++;

                    //If new solution better than the old one
                    if(fitnessMax < sol.fitness()){
                        fitnessMax = sol.fitness();
                    }
                }
            }
            sol.fitness(sol.fitness());

            if (fitnessMax > sol.fitness()) {
                sol.fitness(fitnessMax);
            }else{
                arret = true;
            }
        }
    cout << "Solution : " << sol.fitness() << endl;
    return sol.fitness();
    }
}; 
#endif