#ifndef _RECUIT_H
#define _RECUIT_H

using namespace std;

#include <iostream>
#include <fstream>
#include <random>
#include <vector>
#include <sstream>
#include <utility>
#include <math.h>

#include <base/sample.h>
#include <base/solution.h>
#include <eval/smartEval.h>
#include <rapidjson/document.h>

class RecuitSim : public DFA {
private:
    string _fileName;

public:

    RecuitSim(){}

    vector<string> chararacter(string str){
        vector<string> character;
        istringstream strStream(str);

        for(string element; std::getline(strStream, element, '_'); ){
            character.push_back(move(element));
        }
        return character;
    }

    double run(string fileName, int seed, int _nbEvalMax, double temp, int step, double coeff){
        _fileName = fileName;
        int stepInit = step;
        int nbEval = 0;
        int state = stoi(chararacter(fileName)[1]);

        string url = "/home/lucas/Bureau/Projets_RO/automata-inferenceLucas/instances/" + _fileName;
        Sample sample(url.c_str());

        Solution<double> sol(state, 2);

        //Random engine init using seed. Generates 2 int randoms
        std::mt19937 mt_rand (seed);
        std::uniform_int_distribution<int> dist(0,state-1);     
        std::uniform_int_distribution<int> distribution(1,(state-1));

        for (int i = 0; i < sol.function.size(); ++i) {
            for (int j = 0; j < sol.function[i].size(); ++j) {
                sol.function[i][j] = dist(mt_rand);
            }
        }

        SmartEval eval(mt_rand, sample);

        eval(sol);

        double fitness;
        double fitnessMax;
        bool arret = false;

        while(nbEval < _nbEvalMax && arret == false){
            int solpre;
            int i = rand() % (state);
            int j = rand() % 2;

            solpre = sol.function[i][j];
            fitness = sol.fitness();
            sol.function[i][j] =  (sol.function[i][j] + distribution(mt_rand) ) % state;
            eval(sol);

            if(sol.fitness() - fitness > 0){
                nbEval++;
                step--;
            }else{
                if((double)rand()/((double)RAND_MAX+1) < exp((sol.fitness() - fitness)/temp)){
                }
                else{
                    sol.function[i][j] = solpre;
                    sol.fitness(fitness);
                }
                step--;
                nbEval++;
            }
            if(step == 0){
                temp = temp*coeff;
                step = stepInit;
            }
        }
        cout << "Solution : " << sol.fitness() << endl;
        cout << "Temperature finale : " << temp << endl;
        return sol.fitness();
    }

};
#endif